
class Api:

    def __init__(self, name="api.txt"):
        self.api = open(name, 'w+')

    def send(self, id, x, y):
        self.api.write("{0}: ({1}, {2})\n".format(id, x, y))
